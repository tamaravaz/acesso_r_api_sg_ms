# Notificação de Síndrome Gripal - Elasticsearch API no R

Conexão e extração dos dados do banco epidemiológico de Síndrome Gripal (SG) via api disponibilizada pelo Ministério da saúde.

Elasticsearch API
URL: https://elasticsearch-saps.saude.gov.br/desc-notificacoes-esusve-*/_search?pretty

### Documentação oficial em:
https://opendatasus.saude.gov.br/dataset/casos-nacionais/resource/30c7902e-fe02-4986-b69d-906ca4c2ec36

https://opendatasus.saude.gov.br/dataset/casos-nacionais 
